import { RateLimit, TIME_UNIT } from '@discordx/utilities';
import { ActionRowBuilder, ApplicationCommandOptionType, ButtonBuilder, ButtonInteraction, ButtonStyle, Colors, CommandInteraction, EmbedBuilder, PermissionFlagsBits, Role, roleMention } from 'discord.js';
import { ButtonComponent, Discord, Guard, Slash, SlashOption } from 'discordx';
import { container } from 'tsyringe';
import { CubeStorage } from '../../lib/db/Storage.js';

@Discord()
@Guard(
  RateLimit(TIME_UNIT.seconds, 30, {
    ephemeral: true,
    rateValue: 3
  })
)
export class Ping {
  rolePickers = container.resolve(CubeStorage).rolePickers;

  roleID = '';
  message = '';

  @Slash({
    name: 'ping',
    description: 'Pings a role',
    defaultMemberPermissions: PermissionFlagsBits.SendMessages,
    dmPermission: false
  })
  async ping (
    @SlashOption({
      name: 'role',
      description: 'Role to ping',
      type: ApplicationCommandOptionType.Role,
      required: true
    })
      role: Role,
    @SlashOption({
      name: 'message',
      description: 'Message to send with ping',
      type: ApplicationCommandOptionType.String,
      required: true
    })
      message: string,
      interaction: CommandInteraction
  ) {
    await interaction.deferReply({ ephemeral: true });

    const rolePicker = await this.rolePickers.get(interaction.guildId || '');

    if (rolePicker?.[1]) {
      // check that this is a valid role to ping
      if (rolePicker[1].pingables?.includes(role.id)) {
        // setup the button
        const button = new ButtonBuilder()
          .setLabel('Confirm')
          .setEmoji('✅')
          .setStyle(ButtonStyle.Primary)
          .setCustomId('ping-ok-button');

        const actionRow = new ActionRowBuilder<ButtonBuilder>()
          .addComponents(button);
        // confirm the ping
        await role.guild.members.fetch();
        await interaction.editReply({
          embeds: [
            new EmbedBuilder({
              color: Colors.Green,
              description: `The role ${roleMention(role.id)} includes ${role.members.size.toLocaleString('en-US')} members who will be pinged. Please confirm you want to perform this action.`,
              footer: {
                text: 'Click dismiss to cancel'
              }
            })],
          components: [actionRow]
        });
        this.roleID = role.id;
        this.message = message;
      } else {
        await interaction.editReply({
          embeds: [new EmbedBuilder({
            title: 'Invalid ping',
            color: Colors.Red,
            description: `The role ${roleMention(role.id)} is not pingable.\nThe following roles are pingable however: ${rolePicker[1].pingables?.map(
              x => roleMention(x)
            ).join(',')}`
          })
          ]
        });
      }
    } else {
      await interaction.editReply('Pingables are not enabled on this server');
    }
  }

  @ButtonComponent({ id: 'ping-ok-button' })
  async sendButton(interaction: ButtonInteraction) {
    await interaction.reply({
      embeds: [new EmbedBuilder({
        description: `**${this.message}**`,
        author: {
          name: `${interaction.user.tag} says...`,
          icon_url: interaction.user.displayAvatarURL({ extension: 'png' })
        },
        footer: {
          text: 'You can update your roles with the /roles command to opt in and out of pings for this role'
        },
        color: Colors.Orange
      })],
      content: roleMention(this.roleID),
      allowedMentions: { roles: [this.roleID] }
    });
    // await interaction.reply(`<@&${this.roleID}>\n🗣️ <@${interaction.user.id}> says: "*${this.message}*"`);
  }
}

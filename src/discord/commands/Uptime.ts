import { CommandInteraction, time } from 'discord.js';
import { Discord, Slash } from 'discordx';

@Discord()
export abstract class Uptime {
  @Slash({
    name: 'uptime',
    description: 'show bot current uptime',
    defaultMemberPermissions: 'SendMessages',
    dmPermission: true
  })
  async uptime(interaction: CommandInteraction) {
    await interaction.reply(
      {
        content: `Launched ${time(interaction.client.readyAt, 'R')} at ${time(interaction.client.readyAt)}`,
        ephemeral: true
      }
    );
  }
}

FROM quay.io/cubismod/cubemoji-base:main07c512a0
WORKDIR /usr/src/cubemoji

EXPOSE 7923

COPY assets/ ./assets/

COPY pnpm-lock.yaml .
COPY package.json .

RUN corepack enable && pnpm install --production=true

COPY tsconfig.json .
COPY .eslintrc.json .
COPY src/ ./src/
COPY PRIVACY.md .

RUN pnpm build

CMD ["pnpm", "start"]
